Rajitha's submitted directions:


1. Unzip to a directory .
2. Install Maven to compile.
3. Once Maven is installed do
        - mvn clean
        - mvn install
        -   java -jar ./target/aavri-coding.jar
4. From the browser
    - To get all the branches
          http://localhost:8080/aavri/branches
     - To get branches in a city
          http://localhost:8080/aavri/branches?s=<cityname>
