package com.aavri.coding;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;


import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CodingApplicationTests {

	@Autowired
	private TestRestTemplate testTemplate;

	//TODO : Update this class to test better.
	@Test
	public void testGetBranches() throws Exception{

		ResponseEntity<List> response =
				testTemplate.getForEntity("/aavri/branches",List.class);
		Assertions.assertThat(response.getStatusCode().equals(HttpStatus.OK));
		Assertions.assertThat(response.getBody().size()==1);

	}

	@Test
	public void testGetBranchesByCityName() throws Exception{
		ResponseEntity<List> response =
				testTemplate.getForEntity("/aavri/branches?s=TAD",List.class);
		Assertions.assertThat(response.getStatusCode().equals(HttpStatus.OK));
		Assertions.assertThat(response.getBody().size()==1);

	}

}

