package com.aavri.coding.config;


import com.aavri.coding.util.AavriUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@ComponentScan("com.aavri.coding")
@PropertySource("classpath:static/config.properties")
public class MainConfig {

    @Autowired
    Environment env;

    @Autowired
    public void setEnv(Environment environment) {
        // This is eager initialization
        AavriUtil.setEnv(environment);
    }


}
