package com.aavri.coding.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public final class AavriUtil {

    @Autowired
    private static Environment environment;


    public static void setEnv(Environment env) {
        environment = env;
    }

    public static String getAPIUrl() {
        return environment.getProperty("api.url");
    }

    public static String getBranchesName() {
        return environment.getProperty("branches.name");
    }


}
