package com.aavri.coding.controller;

import java.util.*;

import com.aavri.coding.service.OpenDataHandler;
import com.aavri.coding.util.AavriUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/aavri/branches")
public class BranchController {

   @Autowired
   private  OpenDataHandler dataHandler ;

   private final String  branchURL = AavriUtil.getAPIUrl()+"/"+ AavriUtil.getBranchesName();

   //TODO : If the data is huge it may take considerable time to retrieve it and display,
   // We may need to add offset and limit to optimize the API
    @RequestMapping(method=RequestMethod.GET)
    @ResponseBody
    public List<Map<String,Object>> getBranches(@RequestParam(required=false) String s){
        List<Map<String, Object>> convertedData =getDataAndConvert();
        if (s != null && s.trim().length()>0) {
            convertedData = dataHandler.searchCity(convertedData, s);
        }
        return convertedData;
    }


     private List<Map<String,Object>> getDataAndConvert(){
         String jsonData = dataHandler.retrieveData(branchURL);
         //System.out.println(jsonData);
         List<Map<String, Object>> convertedData = dataHandler.convertData(jsonData);
        return convertedData;
     }



}
