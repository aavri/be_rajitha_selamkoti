package com.aavri.coding.service;

import java.util.List;
import java.util.Map;

public interface OpenDataHandler {

   public String retrieveData(String URL);

    public List<Map<String,  Object>> convertData(String jsonData);

    public List<Map<String, Object>> searchCity(List<Map<String, Object>> convertedData, String searchTerm);
}
