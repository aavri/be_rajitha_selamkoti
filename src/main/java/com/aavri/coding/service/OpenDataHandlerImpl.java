package com.aavri.coding.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

@Service
public class OpenDataHandlerImpl implements OpenDataHandler {

    @Override
    public String retrieveData(String dataURL) {
        StringBuffer sb = new StringBuffer();
        try {
            URL url = new URL(dataURL);
            HttpURLConnection uc = (HttpURLConnection)url.openConnection();
            int statusCode = uc.getResponseCode();
            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream()));
            String tempLine = null;
            while((tempLine = br.readLine()) != null){
                sb.append(tempLine);
            }
            //Need to handle corner cases like what if before close it throws exception
            br.close();
        }catch(Exception e){
            throw new RuntimeException(e);
        }
        return sb.toString();
    }

    @Override
    public List<Map<String, Object>> convertData(String jsonData) {
        List<Map<String, Object>> returnObj = new ArrayList<>();
        Map<String, Object> tempObj = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            TypeReference<Map<String, Object>> typeRef = new TypeReference<Map<String, Object>>() {
            };
            tempObj = mapper.readValue(jsonData, typeRef);
            if (tempObj != null && !tempObj.isEmpty()){
                returnObj =  convertPayload(tempObj);
            }
        } catch(Exception e){
            throw new RuntimeException(e);
        }
        return returnObj;
    }

    private List<Map<String, Object>> convertPayload(Map<String, Object> payload){
        List<Map<String, Object>> returnObj = new ArrayList<>();
        if (payload != null){
            Optional<Object> temp = payload.entrySet().stream()
                    .filter(e -> "data".equalsIgnoreCase(e.getKey()))
                    .map(Map.Entry::getValue)
                    .findFirst();
            //System.out.println(temp);
            List<Map<String, Object>> brand = (List<Map<String, Object>>)temp.get();
            List<Map<String, Object>> brandList =(List<Map<String, Object>>)brand.get(0).get("Brand");
            List<Map<String, Object>> branches = (List<Map<String, Object>>)brandList.get(0).get("Branch");
            //System.out.println(branches);
            returnObj = customizePayload(branches);
        }
        return returnObj;
    }

    private List<Map<String, Object>> customizePayload(List<Map<String, Object>> branches){
        List<Map<String, Object>> returnObj = new ArrayList<>();
        if (branches != null && !branches.isEmpty()){
            for (Map<String, Object> branch: branches){
                Map<String, Object> tempMap = new LinkedHashMap<>();
                tempMap.put("branchName", branch.get("Name"));
                tempMap.putAll(retrievePostalAddress(branch));
                returnObj.add(tempMap);
            }
        }
        return returnObj;
    }

    private Map<String, Object> retrievePostalAddress(Map<String, Object> branch){
        Map<String, Object> returnObj = new LinkedHashMap<>();
        Map<String, Object> postalObj = (Map<String, Object>)branch.get("PostalAddress");
        Map<String, String> geoDetails = (Map<String, String>)((Map<String, Object>)postalObj.get("GeoLocation")).get("GeographicCoordinates");
        String latitude = geoDetails.get("Latitude");
        String longitude = geoDetails.get("Longitude");
        if (latitude != null){
            returnObj.put("latitude", Float.parseFloat(latitude));
        } else {
            returnObj.put("latitude", 0.0);
        }
        if (longitude != null){
            returnObj.put("longitude", Float.parseFloat(longitude));
        } else {
            returnObj.put("longitude", 0.0);
        }
        String addressLine = ((List<String>)postalObj.get("AddressLine")).get(0);
        returnObj.put("streetAddress", addressLine);
        returnObj.put("city", postalObj.get("TownName"));
        Object countrySubObj = postalObj.get("CountrySubDivision");
        if (countrySubObj != null) {
            String countrySubDivision = ((List<String>) countrySubObj).get(0);
            returnObj.put("countrySubDivision", countrySubDivision);
        }
        returnObj.put("country", postalObj.get("Country"));
        returnObj.put("postCode",postalObj.get("PostCode"));
        return returnObj;
    }


    //TODO :doing search on converted data but to optimize it
    // we need to search for the city on raw data and then convert it.
    @Override
    public List<Map<String, Object>> searchCity(List<Map<String,  Object>> convertedData, String searchTerm){
        List<Map<String, Object>> result = new ArrayList<>();
        if (convertedData != null && !convertedData.isEmpty()){
            /*result = convertedData.parallelStream()
                    .map(h -> h.entrySet().stream()
                            .filter(entry -> entry.getKey().equalsIgnoreCase("city")
                                    && entry.getValue() != null
                                    && ((String)entry.getValue()).toLowerCase().contains(searchTerm.toLowerCase()))
                            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)))
                    .collect(Collectors.toList());
             */
            for (Map<String, Object> temp : convertedData){
                Object cityObj = temp.get("city");
                if (cityObj != null){
                    String city = (String)cityObj;
                    if (city.toLowerCase().contains(searchTerm.toLowerCase())){
                        result.add(temp);
                    }
                }
            }
        }
        return result;
    }


}
